#!/bin/bash
# Usage:
# For new install:
# ./obm_post_install.sh
#
# For updating an existing system
# ./obm_post_install.sh update [mapserver sablon supervisor sql]

source .env

if test $# -eq 0; then
    update=none
else
    update=$2
fi

if [ "$update" = "mapserver" ]; then

    docker-compose exec -T mapserver bash -c "msencrypt -keygen /var/lib/openbiomaps/maps/access.key"

elif [ "$update" = "sablon" ]; then

    sablon_password_hash=$(docker-compose exec -T mapserver bash -c "msencrypt -key /var/lib/openbiomaps/maps/access.key $SABLON_ADMIN_PASSWORD | tr -d '\n'")
    docker-compose exec -T app bash -c "sed -i 's/ password={.*} / password={$sablon_password_hash} /' /var/www/html/biomaps/root-site/projects/sablon/private/private.map"

elif [ "$update" = "supervisor" ]; then

    supervisor_password=$(< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c 32)
    echo -e "\nUse the following password for Supervisor:\n\n$supervisor_password\n\nKeep it safe!"
    docker-compose exec -T app bash -c "htpasswd -b /etc/openbiomaps/.htpasswd supervisor $supervisor_password"

elif [ "$update" = "sql" ]; then

    docker-compose exec -T biomaps_db bash -c "psql -U biomapsadmin gisdata -c \"ALTER ROLE biomapsadmin WITH PASSWORD '$BIOMAPS_DB_PASSWORD';\""
    docker-compose exec -T biomaps_db bash -c "psql -U biomapsadmin gisdata -c \"ALTER ROLE sablon_admin WITH PASSWORD '$SABLON_ADMIN_PASSWORD';\""
    docker-compose exec -T biomaps_db bash -c "psql -U biomapsadmin biomaps -c \"ALTER ROLE biomapsadmin WITH PASSWORD '$BIOMAPS_DB_PASSWORD';\""
    docker-compose exec -T biomaps_db bash -c "psql -U biomapsadmin biomaps -c \"ALTER ROLE mainpage_admin WITH PASSWORD '$MAINPAGE_ADMIN_PASSWORD';\""

elif [ "$update" = "none" ]; then

    # New installation commands:

    # SQL access
    echo -e "Set passwords for sql users in biomaps_db image"
    docker-compose exec -T biomaps_db bash -c "psql -U biomapsadmin gisdata -c \"ALTER ROLE biomapsadmin WITH PASSWORD '$BIOMAPS_DB_PASSWORD';\""
    docker-compose exec -T biomaps_db bash -c "psql -U biomapsadmin gisdata -c \"ALTER ROLE sablon_admin WITH PASSWORD '$SABLON_ADMIN_PASSWORD';\""
    docker-compose exec -T biomaps_db bash -c "psql -U biomapsadmin biomaps -c \"ALTER ROLE biomapsadmin WITH PASSWORD '$BIOMAPS_DB_PASSWORD';\""
    docker-compose exec -T biomaps_db bash -c "psql -U biomapsadmin biomaps -c \"ALTER ROLE mainpage_admin WITH PASSWORD '$MAINPAGE_ADMIN_PASSWORD';\""

    # Generate mapserver access.key for password encryption
    docker-compose exec -T mapserver bash -c "msencrypt -keygen /var/lib/openbiomaps/maps/access.key"
    #access_key=$(docker-compose exec -T mapserver bash -c "cat /var/lib/openbiomaps/maps/access.key")

    # Set sablon project's map file
    sablon_password_hash=$(docker-compose exec -T mapserver bash -c "msencrypt -key /var/lib/openbiomaps/maps/access.key $SABLON_ADMIN_PASSWORD | tr -d '\n'")
    docker-compose exec -T app bash -c "sed -i 's/\*\*\* ChangeThisPasswordHash \*\*\*/$sablon_password_hash/' /var/www/html/biomaps/root-site/projects/sablon/private/private.map"
    
    # For Supervisor access for system_vars
    docker-compose exec -T app bash -c "chown www-data /etc/openbiomaps/system_vars.php.inc"

    # Supervisor password
    supervisor_password=$(< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c 32)
    echo -e "\nUse the following password for Supervisor:\n\n$supervisor_password\n\nKeep it safe!"
    docker-compose exec -T app bash -c "htpasswd -b /etc/openbiomaps/.htpasswd supervisor $supervisor_password"

else

    echo -e "Bad arguments supplied!\nNothing happened."

fi
