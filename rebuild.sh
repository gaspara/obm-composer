#! /bin/bash

SERVICE=$1
#docker-compose pull $SERVICE
docker-compose stop $SERVICE
docker-compose rm -v -f $SERVICE
docker-compose up -d $SERVICE
docker-compose logs -f $SERVICE 
