#!/bin/bash
# Run this script before install (before the docker-compose up -d command!) a new docker system

# Create & Set postgres password
postgres_password=$(< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c${1:-32})
printf "BIOMAPS_POSTGRES_PASSWORD=$postgres_password\nGISDATA_POSTGRES_PASSWORD=$postgres_password\n" > .env

# Create & set admin passwords
biomaps_password=$(< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c 32)
printf "BIOMAPS_DB_PASSWORD=$biomaps_password\n" >> .env 
mainpage_admin_password=$(< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c 32)
printf "MAINPAGE_ADMIN_PASSWORD=$mainpage_admin_password\n" >> .env 
sablon_password=$(< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c 32)
printf "SABLON_ADMIN_PASSWORD=$sablon_password\n" >> .env 
sablon_hash=$(< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c 8)
printf "SABLON_HASH=$sablon_hash\n" >> .env

# Update default service port if needed
if test $# -eq 0; then
    set_port=0
else
    set_port=$1
fi

if [ "$set_port" != "0" ]; then
    #im_dashboard default:
    #set_port=9890
    sed -i "s/- 9890:80/- $set_port:80/" docker-compose.yml
fi
